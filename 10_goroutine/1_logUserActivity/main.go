package main

import (
	"fmt"
	"math/rand"
	"os"
	"sync"
	"time"
)

var actions = []string{
	"logged in",
	"logged out",
	"create record",
	"delete record",
	"update record",
}

// создание юзеров и запись их действий (логирование) в файл

type logItem struct {
	action    string
	timestamp time.Time
}

type User struct {
	id    int
	email string
	logs  []logItem
}

func (u User) getActivityInfo() string {
	out := fmt.Sprintf("ID: %d | Email: %s\nActivity log:\n", u.id, u.email)
	for i, val := range u.logs {
		out += fmt.Sprintf("%d. [%s] at %s \n", i, val.action, val.timestamp)
	}
	return out
}

var wg = &sync.WaitGroup{}

func main() {
	rand.Seed(time.Now().UnixNano())

	timeStart := time.Now()

	users := generateUsers(200)
	for _, user := range users {
		wg.Add(1)
		go saveUserInfo(user)

	}
	wg.Wait()
	fmt.Println("TIME ELAPSED: ", time.Since(timeStart).Seconds())
}

func saveUserInfo(user User) error {
	time.Sleep(time.Millisecond * 10)
	fmt.Printf("writing file for user id: %d\n", user.id)
	fileName := fmt.Sprintf("10_goroutine/1_logUserActivity/logs/uid_%d.txt", user.id)
	file, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, 0644)
	_, err = file.WriteString(user.getActivityInfo())
	wg.Done()
	return err
}

func generateUsers(count int) []User {
	users := make([]User, count)
	for i := 0; i < count; i++ {
		users[i] = User{
			id:    i + 1,
			email: fmt.Sprintf("tut%s@nin.by", i+1),
			logs:  generateLogs(rand.Intn(500) + 100),
		}
		time.Sleep(time.Millisecond * 100)
	}
	return users
}

func generateLogs(count int) []logItem {
	logs := make([]logItem, count)
	for i := 0; i < count; i++ {
		logs[i] = logItem{
			action:    actions[rand.Intn(len(actions))],
			timestamp: time.Now(),
		}
	}
	return logs
}
