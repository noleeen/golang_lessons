package main

import "fmt"

func main() {

	m := make(chan string, 3)

	//go func() {
	//	m <- "offer "
	//	m <- "offer " +
	//		"2023"
	//}()

	m <- "1"
	m <- "2"
	m <- "3"
	close(m)

	//for {
	//	val, open := <-m
	//	if !open {
	//		fmt.Println("channel closed")
	//		break
	//	}
	//	fmt.Println(val)
	//}

	//это тожу самое что и верхний цикл (синтаксический сахар)
	for val := range m {
		fmt.Println(val)
	}

}
