package main

import (
	"fmt"
	"time"
)

func main() {
	m1 := make(chan string)
	m2 := make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 200)
			m1 <- "came 200 milliseconds"
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second)
			m2 <- "came a second"
		}
	}()

	//for {
	//	fmt.Println(<-m1)
	//	fmt.Println(<-m2)
	//}
	//тут по очереди считывается с одного канала потом с другого из-за блокировок,
	//но с помощью select можно с m1 успеть читать 5 раз пока с m2 будет прочит 1 раз, так как select не блокирует работу

	for {
		select {
		case m := <-m1:
			fmt.Println(m)
		case m := <-m2:
			fmt.Println(m)
		}
	}
}
