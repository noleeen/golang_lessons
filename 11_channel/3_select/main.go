package main

import (
	"fmt"
	"sync"
)

// ЗАПИСЫВАЕМ В СЕЛЕКТЕ ПОКА ИЗ DATA КТО-ТО ЧИТАЕТ. КАК ТОЛЬКО ПРИХОДИТ ЧТО-ТО В EXIT ВЫХОДИМ ИЗ ФУНКЦИИ
//func main() {
//	data := make(chan string, 10)
//	exit := make(chan string)
//
//	go func() {
//		for i := 0; i < 10; i++ {
//			fmt.Println(<-data)
//		}
//		exit <- ""
//	}()
//	countTo(data, exit)
//}
//
//func countTo(d, exit chan string) {
//	x := "hi! "
//	for {
//		select {
//		case d <- x:
//			x += "i am going to"
//
//		case <-exit:
//			fmt.Println("end work")
//			return
//		}
//
//	}
//}

//---------------------------------------------------------------------------------------
// в select считываем данные из data пока они туда приходят. затем когда данные приходят в exit выходим из функции
var wg sync.WaitGroup

func main() {
	data := make(chan string, 10)
	exit := make(chan string)

	go func() {
		for i := 0; i < 10; i++ {
			wg.Add(1)
			data <- fmt.Sprint(i)

		}
		wg.Wait()
		exit <- ""
	}()
	countTo(data, exit)
}

func countTo(d, exit chan string) {
	for {
		select {
		case x := <-d:
			fmt.Println(x)
			wg.Done()

		case <-exit:
			fmt.Println("end work")
			return
		}

	}
}
