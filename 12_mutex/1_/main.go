package main

import (
	"fmt"
	"sync"
)

type Counter struct {
	mu    sync.Mutex
	count map[string]int
}

func (c *Counter) Inc(key string) {
	c.mu.Lock()
	c.count[key]++
	c.mu.Unlock()
}

func (c *Counter) Val(key string) int {
	c.mu.Lock()
	defer c.mu.Unlock()

	return c.count[key]
}

func main() {
	key := "t"
	obj := Counter{count: make(map[string]int)} //инициализировали пустую мапу, мьютекс оставили по дефолту

	for i := 0; i < 1000; i++ {
		go obj.Inc(key)
	}

	//time.Sleep(time.Second)
	fmt.Println(obj.Val(key))
}
