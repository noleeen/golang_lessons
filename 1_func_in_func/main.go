package main

import (
	"fmt"
)

func main() {
	sum := func(x, y int) int {
		return x + y
	}
	mult := func(x, y int) int {
		return x * y
	}

	fmt.Println(headerFunction(sum, "sum"))
	fmt.Println(headerFunction(mult, "multiply"))

}

func headerFunction(callBack func(int, int) int, n string) int {
	res := callBack(5, 2)
	fmt.Println(n)
	return res

}
