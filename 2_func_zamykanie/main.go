package main

import "fmt"

func balance(all int) func(int) int {
	fmt.Printf("привезли %d штук 21 июня 2123г\n", all)
	stockBalance := all

	return func(sold int) int {
		stockBalance -= sold
		fmt.Printf("продано %d штук. на складе осталось %d штук.\n", sold, stockBalance)
		return stockBalance
	}
}

func main() {
	amount_watches := balance(24)
	amount_watches(3)
	amount_watches(1)
	amount_watches(4)

}
