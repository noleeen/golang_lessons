// Proxy смотрит хватает ли прав доступа у пользователя и исходя из этого предоставляет данные
//(из базы данных dataBase) или нет. При этом запрос не задействует базу данных, а обрабатывается тольько Proxy,
//и уже если Proxy даёт добро, то запрос идёт к базе данных

package main

import (
	"fmt"
	"gitlab.com/Golang_lesson/30_Patterns/1_Proxy/pkg"
)

var (
	admin = "administrator"
	user  = "user"
	users = map[string]bool{
		admin: true,
		user:  false,
	}
)

func main() {
	proxy := pkg.ProxyDataBase{
		Users: users,
		Db:    &pkg.DataBase{},
	}

	requestAdmin, err := proxy.Get(admin)
	fmt.Printf("from %s got data: %v \nErr: %v\n", admin, requestAdmin, err)

	requestUser, err := proxy.Get(user)
	fmt.Printf("from %s got data: %v \nErr: %v\n", user, requestUser, err)

}
