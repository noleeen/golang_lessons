package pkg

import "errors"

type ProxyDataBase struct {
	Users map[string]bool
	Db    *DataBase
}

func (p ProxyDataBase) Get(user string) ([]string, error) {
	if !p.Users[user] {
		return nil, errors.New("insufficient access rights")
	}
	return p.Db.Get(user)
}
