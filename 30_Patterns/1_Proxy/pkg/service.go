package pkg

type Service interface {
	Get(user string) ([]string, error)
}
