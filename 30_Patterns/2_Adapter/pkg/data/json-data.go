package data

type JsonDocument struct {
}

func (doc JsonDocument) ConvertToXml() string {
	return "<xml></xml>"
}

type JsonDocumentAdaptor struct {
	jsonDocument *JsonDocument
}

func (adapter JsonDocumentAdaptor) SendXmlData() {
	adapter.jsonDocument.ConvertToXml()
	println("send XML data")
}
