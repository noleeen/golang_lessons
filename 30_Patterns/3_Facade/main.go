package main

import (
	"fmt"
	"gitlab.com/Golang_lesson/30_Patterns/3_Facade/pkg"
)

//добавляем банк, две карты, приыяязанные к этому банку, двух пользователей привязанных  к картам,
//продукт и магазин в котором есть данный продукт
var (
	bank = pkg.Bank{
		Name:  "Bank12",
		Cards: []pkg.Card{},
	}
	card1 = pkg.Card{
		Name:    "CRD - 1",
		Balance: 200,
		Bank:    &bank,
	}
	card2 = pkg.Card{
		Name:    "CRD - 2",
		Balance: 5,
		Bank:    &bank,
	}

	user1 = pkg.User{
		Name: "User-1",
		Card: &card1,
	}
	user2 = pkg.User{
		Name: "User-2",
		Card: &card2,
	}

	prod = pkg.Product{
		Name:  "chees",
		Price: 150,
	}

	shop = pkg.Shop{
		Name: "shop1",
		Products: []pkg.Product{
			prod,
		},
	}
)

func main() {
	println("[bank] produce cards")
	bank.Cards = append(bank.Cards, card2, card1)
	fmt.Printf("[%s]", user1.Name)
	err := shop.Sell(user1, prod.Name)
	if err != nil {
		println(err.Error())
		return
	}

	fmt.Printf("[%s]", user2.Name)
	err = shop.Sell(user2, prod.Name)
	if err != nil {
		println(err.Error())
		return
	}

}
