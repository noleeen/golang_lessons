package pkg

import (
	"errors"
	"fmt"
	"time"
)

type Bank struct {
	Name  string
	Cards []Card
}

//банк приходит запрос на проверку баланса
func (b Bank) CheckBalance(cardNumber string) error {
	println(fmt.Sprintf("[bank] getting card's balance %s", cardNumber))
	time.Sleep(time.Second)
	for _, card := range b.Cards {
		if card.Name != cardNumber {
			continue
		}
		if card.Balance <= 0 {
			return errors.New("[bank] insufficient funds on credit card")
		}
	}
	println("[bank] positive balance ")
	return nil
}
