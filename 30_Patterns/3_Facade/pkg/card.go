package pkg

import "time"

type Card struct {
	Name    string
	Balance float64
	Bank    *Bank
}

//карта делает запрос банку на проверку баланса
func (c Card) CheckBalance() error {
	println("[card] request to bank for balance check ")
	time.Sleep(time.Second)
	return c.Bank.CheckBalance(c.Name)
}
