package pkg

import (
	"errors"
	"fmt"
	"time"
)

type Shop struct {
	Name     string
	Products []Product //ассортимент товаров
}

//в магазин приходит пользователь и пытается купить товар
//функция Sell это фасад: тут происходит основное взаимодействие с другими сервисами
func (shop Shop) Sell(user User, product string) error {
	println("[shop] request to user, for getting card's balance")
	time.Sleep(time.Second)
	err := user.Card.CheckBalance()
	if err != nil {
		return err
	}

	fmt.Printf("[shop] check - can user [%s] to byu product!\n", user.Name)
	time.Sleep(time.Second)
	for _, prod := range shop.Products {
		if prod.Name != product {
			continue
		}
		if prod.Price > user.GetBalance() {
			return errors.New("[shop] insufficine funds to buy product! ")
		}
		fmt.Printf("[shop] product [%s] - bought\n", prod.Name)

	}
	return nil
}
