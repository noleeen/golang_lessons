package main

//https://www.youtube.com/watch?v=dPN0IUyKV-4&list=PLxj7Nz8YYkVW5KHnsb9qWUDP2eD1TXl1N&index=4

import "gitlab.com/Golang_lesson/30_Patterns/4_Abstruct factory/pkg"

var (
	brands = []string{"Asus", "Hp", "dell"}
)

func main() {
	for _, brand := range brands {
		factory, err := pkg.GetFactory(brand)
		if err != nil {
			println(err.Error())
			continue
		}
		monitor := factory.GetMonitor()
		monitor.PrintDetails()

		computer := factory.GetComputer()
		computer.PrintDetails()
	}
}
