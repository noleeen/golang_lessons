package pkg

import "fmt"

type AsusComp struct {
	Memory int
	Cpu    int
}

func (pc AsusComp) PrintDetails() {
	fmt.Printf("[Asus] Pc Cpu:[%d] Mem:[%d]\n", pc.Cpu, pc.Memory)
}
