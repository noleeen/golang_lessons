package pkg

import "fmt"

type HpComp struct {
	Memory int
	Cpu    int
}

func (pc HpComp) PrintDetails() {
	fmt.Printf("[Hp] Pc Cpu:[%d] Mem:[%d]\n", pc.Cpu, pc.Memory)
}
