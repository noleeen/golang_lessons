package pkg

type HpFactory struct {
}

func (factory HpFactory) GetComputer() Computer {
	return HpComp{
		Memory: 16,
		Cpu:    4,
	}
}

func (factory HpFactory) GetMonitor() Monitor {
	return HpMonitor{Size: 24}
}
