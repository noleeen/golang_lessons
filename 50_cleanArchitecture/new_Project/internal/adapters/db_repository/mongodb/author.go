package mongodb

import (
	"context"
	"gitlab.com/Golang_lesson/50_cleanArchitecture/new_Project/internal/domain/entity"
	"go.mongodb.org/mongo-driver/mongo"
)

type authorStorage struct {
	db *mongo.Database
}

func NewAuthorStorage(db *mongo.Database) *authorStorage {
	return &authorStorage{db: db}
}

func (bs *authorStorage) GetOne(id string) *entity.Author {
	return nil
}
func (bs *authorStorage) GetAll(ctx context.Context) []*entity.Author {
	return nil
}
func (bs *authorStorage) Create(book *entity.Author) *entity.Author {
	return nil
}
func (bs *authorStorage) Delete(book *entity.Author) error {
	return nil
}
