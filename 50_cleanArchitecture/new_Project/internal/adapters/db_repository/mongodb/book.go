package mongodb

import (
	"context"
	"gitlab.com/Golang_lesson/50_cleanArchitecture/new_Project/internal/domain/entity"
	"go.mongodb.org/mongo-driver/mongo"
)

type bookStorage struct {
	db *mongo.Database
}

func NewBookStorage(db *mongo.Database) *bookStorage {
	return &bookStorage{db: db}
}

func (bs *bookStorage) GetOne(id string) *entity.Book {
	return nil
}
func (bs *bookStorage) GetAll(ctx context.Context) []*entity.Book {
	return nil
}
func (bs *bookStorage) Create(book *entity.Book) *entity.Book {
	return nil
}
func (bs *bookStorage) Delete(book *entity.Book) error {
	return nil
}
