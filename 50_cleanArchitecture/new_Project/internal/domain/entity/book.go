package entity

import "fmt"

type Book struct {
	UUID   string `json:"uuid,omitempty"`
	Name   string `json:"name,omitempty"`
	Year   int    `json:"year,omitempty"`
	Author Author `json:"author,omitempty"`
	Gengre Genre  `json:"gengre,omitempty"`
	Busy   bool   `json:"busy,omitempty"`
	Owner  string `json:"owner,omitempty"`
}

func (b *Book) Take(owner string) error {
	if b.Busy {
		return fmt.Errorf("Book %s is busy", b.Name)
	}
	b.Owner = owner
	b.Busy = true
	return nil
}
