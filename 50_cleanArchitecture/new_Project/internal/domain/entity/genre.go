package entity

type Genre struct {
	UUID string `json:"uuid,omitempty"`
	Name string `json:"name,omitempty"`
}
