package service

import (
	"context"
	"gitlab.com/Golang_lesson/50_cleanArchitecture/new_Project/internal/domain/entity"
)

type BookStorage interface {
	GetOne(uuid string) *entity.Book
	GetAll(ctx context.Context) []*entity.Book
	Create(book *entity.Book) *entity.Book
	Delete(book *entity.Book) error
}

type bookService struct {
	storage BookStorage
}

func NewBookService(storage BookStorage) *bookService {
	return &bookService{storage: storage}
}

func (s *bookService) Create(ctx context.Context) *entity.Book {
	return nil
}

func (s *bookService) GetByUUID(ctx context.Context, id string) *entity.Book {
	return s.storage.GetOne(id)
}

func (s *bookService) GetAll(ctx context.Context) []*entity.Book {
	return s.storage.GetAll(ctx)
}
