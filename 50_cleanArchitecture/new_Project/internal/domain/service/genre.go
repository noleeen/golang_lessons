package service

import (
	"context"
	"gitlab.com/Golang_lesson/50_cleanArchitecture/new_Project/internal/domain/entity"
)

type GenreStorage interface {
	GetOne(uuid string) *entity.Genre
	GetAll(ctx context.Context) []*entity.Genre
	Create(book *entity.Genre) *entity.Genre
	Delete(book *entity.Genre) error
}

type genreService struct {
	storage GenreStorage
}

func NewGenreService(storage GenreStorage) *genreService {
	return &genreService{storage: storage}
}

func (s *genreService) Create(ctx context.Context) *entity.Genre {
	return nil
}

func (s *genreService) GetByUUID(ctx context.Context, id string) *entity.Genre {
	return s.storage.GetOne(id)
}

func (s *genreService) GetAll(ctx context.Context) []*entity.Genre {
	return s.storage.GetAll(ctx)
}
