package main

import "fmt"

func main() {
	ar := []int{4, 5, 7, 8, 9}
	sum := func(x, y int) int { return x + y }
	mult := func(x, y int) int { return x * y }

	fmt.Println(Reduce(ar, sum, 0))
	fmt.Println(Reduce(ar, mult, 1))
}

func Reduce[T any](list []T, accumulator func(T, T) T, init T) T {
	for _, el := range list {
		init = accumulator(init, el)
	}
	return init
}
