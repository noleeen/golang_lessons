package main

import (
	"fmt"
	"golang.org/x/exp/constraints"
)

func main() {
	var x, y float32
	x = 10
	y = 10.1
	fmt.Println(max(x, y))                        // сравниваем float
	fmt.Println(max(15, 1))                       // сравниваем int
	fmt.Println(max("asdf", "1_logUserActivity")) // сравниваем strings
	fmt.Println(max("asdf", "zsdf"))              // сравниваем strings (сравнило по лексикографическим свойствам)

	array := []string{"sdf", "3452", "4"}
	fmt.Println(isExist(array, "4"))
}

func max[T constraints.Ordered](x, y T) T {
	if x > y {
		return x
	}
	return y
}

func isExist[V comparable](ar []V, found V) bool {
	for _, el := range ar {
		if el == found {
			return true
		}
	}
	return false
}
