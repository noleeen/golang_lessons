package main

import "fmt"

type User struct {
	name  string
	phone string
	age   int
}

func main() {
	users := []User{
		{
			name:  "Ma",
			phone: "123",
			age:   12,
		},
		{
			name:  "Man",
			phone: "1234",
			age:   122,
		},
	}
	a := []int{44, 33, 15}
	s := []string{"44", "sdf33", "15"}
	fmt.Println(existIn(a, 4))       //поиск в слайсе int
	fmt.Println(existIn(s, "44"))    //поиск в слайсе string
	fmt.Println(existIn(users, User{ //поиск в слайсе с типом структуры User
		name:  "Man",
		phone: "1234",
		age:   122,
	}))
	showEl(a)
	showEl(s)
	showEl(users)

}

func existIn[V comparable](ar []V, find V) bool {
	for _, el := range ar {
		if el == find {
			return true
		}
	}
	return false
}

func showEl[T comparable](ar []T) {
	fmt.Println(ar)
}
