package main

import "fmt"

// a := []int {1_logUserActivity, 2, 3}
var b = []float32{1.1, 1, 2.2}

func main() {
	a := []int{1, 2, 3}
	b := []float32{1.1, 2, 5.5}

	fmt.Println(sum(b), sum(a))

}

// функция которая благодаря дженерикам считает сумму чисел для двух типов данных
func sum[V int | float32](ar []V) V { // тут используется "тип параметры" в квадратных скобках - это на самом деле интерфейс!
	var sum V
	for _, num := range ar {
		sum += num

	}
	return sum
}

// МОЖНО ЗАПИСАТЬ С ПОМ ИНТЕРФЕЙСА
// type Num interface{
// 	int | float64
// }
// func sum [V Num](ar[] V) V{

// }
//+++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++
