package main

import "fmt"

func main() {
	fmt.Println(Ternary(true, 10, 20))

	fmt.Println(Max(22, 4))
}

func Max(x, y int) int { return Ternary(x > y, x, y) } // с помощью тернарного оператора реализуем ф-ию поиска большего числа

func Ternary[V any](cond bool, x, y V) V { // тернарный оператор
	if cond {
		return x
	}
	return y
}
