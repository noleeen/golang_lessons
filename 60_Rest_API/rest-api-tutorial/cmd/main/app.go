package main

import (
	"context"
	"fmt"
	"github.com/julienschmidt/httprouter"
	author2 "gitlab.com/Golang_lesson/60_Rest_API/rest-api-tutorial/internal/author"
	author "gitlab.com/Golang_lesson/60_Rest_API/rest-api-tutorial/internal/author/db"
	"gitlab.com/Golang_lesson/60_Rest_API/rest-api-tutorial/internal/config"
	"gitlab.com/Golang_lesson/60_Rest_API/rest-api-tutorial/internal/user"
	"gitlab.com/Golang_lesson/60_Rest_API/rest-api-tutorial/pkg/client/postgresql"
	"gitlab.com/Golang_lesson/60_Rest_API/rest-api-tutorial/pkg/logging"
	"net"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"time"
)

func main() {
	logger := logging.GetLogger()

	logger.Info("create router")
	router := httprouter.New()

	cfg := config.GetConfig()

	// MongoDB------------------------------------
	//cM := cfg.MongoDB
	//mongoDBClient, err := mongodb.NewClient(context.Background(), cM.Host, cM.Port, cM.Username, cM.Password, cM.Database, cM.AuthDB)
	//if err != nil {
	//	panic(err)
	//}
	//
	//storage := db.NewStorage(mongoDBClient, cfg.MongoDB.Collection, logger)
	//
	//user1 := user.User{
	//	ID:           "",
	//	Email:        "m@tut.by",
	//	Username:     "Nina",
	//	PasswordHash: "12345",
	//}
	//user1ID, err := storage.Create(context.Background(), user1)
	//if err != nil {
	//	panic(err)
	//}
	//logger.Info(user1ID)
	// MongoDB------------------------------------

	//PostgreSQL----------------------------------
	postgresqlClient, err := postgresql.NewClient(context.TODO(), 3, *cfg)
	if err != nil {
		logger.Fatal(err)
	}
	repository := author.NewRepository(postgresqlClient, logger)

	auth := author2.Author{
		Name: "Sasha H.",
	}

	err = repository.Create(context.TODO(), &auth)
	if err != nil {
		logger.Fatal(err)
	}
	logger.Infof("%v", auth)

	all, err := repository.FindAll(context.TODO())
	if err != nil {
		logger.Fatal(err)
	}
	for _, ath := range all {
		logger.Info(ath)
	}

	//PostgreSQL----------------------------------

	logger.Info("register user handler")
	handler := user.NewHandler(*logger)
	handler.Register(router)

	start(router, cfg)

}

func start(router *httprouter.Router, cfg *config.Config) {
	//запускаем сервер с помощью пакета net

	logger := logging.GetLogger()
	logger.Info("start application")

	var listener net.Listener
	var listenError error

	if cfg.Listen.Type == "sock" {
		logger.Info("detect app path")
		appDir, err := filepath.Abs(filepath.Dir(os.Args[0])) //получаем путь папки в которой находимся
		if err != nil {
			logger.Fatal(err)
		}
		logger.Info("create socket")
		socketPath := path.Join(appDir, "app.sock")

		logger.Info("listen unix socket")
		listener, listenError = net.Listen("unix", socketPath)
		logger.Infof("server is listening unix socket %s", socketPath)
	} else {
		logger.Info("listen tcp")
		listener, listenError = net.Listen("tcp", fmt.Sprintf("%s:%s", cfg.Listen.BindIP, cfg.Listen.Port))
		logger.Infof("server is listening port %s:%s", cfg.Listen.BindIP, cfg.Listen.Port)
	}
	if listenError != nil {
		logger.Fatal(listenError)
	}

	//создаём ссылку на сервер
	server := &http.Server{
		Handler:      router,
		WriteTimeout: 15 * time.Second, // делаем таймаут на запись 15сек
		ReadTimeout:  15 * time.Second, // делаем таймаут на чтение 15сек
	}

	logger.Fatal(server.Serve(listener))

}
