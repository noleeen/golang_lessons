CREATE TABLE public.author
(
    id   UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    name VARCHAR(100) NOT NULL
);

CREATE TABLE public.book(
                            id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
                            name VARCHAR(100) NOT NULL,
                            author_id UUID NOT NULL,
                            CONSTRAINT author_fk FOREIGN KEY (author_id) REFERENCES public.author(id)
);



INSERT INTO author (name) VALUES ('народ');    --53b32657-1bc6-4dd4-98e6-09f64a5829e1
INSERT INTO author (name) VALUES ('Алесь Снег'); --9cbaf491-737c-4e95-b28c-9a9dc0d0c468
INSERT INTO author (name) VALUES ('Виктар Мартинович'); --6eb4a5f9-5afc-4842-aefa-132bb1e77168

INSERT INTO book (name, author_id) VALUES ('колобок', '53b32657-1bc6-4dd4-98e6-09f64a5829e1') ;
INSERT INTO book (name, author_id) VALUES ('cytu', '53b32657-1bc6-4dd4-98e6-09f64a5829e1');
INSERT INTO book (name, author_id) VALUES ('Сядзиба', '9cbaf491-737c-4e95-b28c-9a9dc0d0c468');
INSERT INTO book (name, author_id) VALUES ('Ноч', '6eb4a5f9-5afc-4842-aefa-132bb1e77168');

INSERT INTO book (name, author_id) VALUES ('МОва', '6eb4a5f9-5afc-4842-aefa-132bb1e77168');