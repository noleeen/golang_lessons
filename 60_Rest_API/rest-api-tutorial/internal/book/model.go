package book

import "gitlab.com/Golang_lesson/60_Rest_API/rest-api-tutorial/internal/author"

type Book struct {
	ID      string          `json:"id"`
	Name    string          `json:"name"`
	Age     int             `json:"age"`
	Authors []author.Author `json:"authors"`
}
