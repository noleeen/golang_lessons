package config

//cleanEnv конфигурация https://www.youtube.com/watch?v=asLUNpndGj0

import (
	"github.com/ilyakaznacheev/cleanenv"
	"gitlab.com/Golang_lesson/60_Rest_API/rest-api-tutorial/pkg/logging"
	"sync"
)

//MONGO----------------------------
//type Config struct {
//	IsDebug *bool `yaml:"is_debug" env-required:"true"` //env-required - обязателдьное поле
//	Listen  struct {
//		Type   string `yaml:"type" env-default:"port"` // env-default - значение по умолчанию
//		BindIP string `yaml:"bind_ip" env-default:"127.0.0.1"`
//		Port   string `yaml:"port" env-default:"8080"'`
//	} `yaml:"listen"`
//	MongoDB struct {
//		Host       string `json:"host"`
//		Port       string `json:"port"`
//		Database   string `json:"database"`
//		AuthDB     string `json:"authDB"`
//		Username   string `json:"username"`
//		Password   string `json:"password"`
//		Collection string `json:"collection"`
//	} `json:"mongodb"`
//}
//MONGO----------------------------

//POSTGRESQL----------------------------
//type Config struct {
//	IsDebug *bool `yaml:"is_debug" env-required:"true"` //env-required - обязателдьное поле
//	Listen  struct {
//		Type   string `yaml:"type" env-default:"port"` // env-default - значение по умолчанию
//		BindIP string `yaml:"bind_ip" env-default:"127.0.0.1"`
//		Port   string `yaml:"port" env-default:"8080"'`
//	} `yaml:"listen"`
//	Storage StorageConfig `yaml:"storage"`
//}
//
//type StorageConfig struct {
//	Host     string `json:"host"`
//	Port     string `json:"port"`
//	Database string `json:"database"`
//	Username string `json:"username"`
//	Password string `json:"password"`
//}
//POSTGRESQL----------------------------

type Config struct {
	IsDebug *bool `yaml:"is_debug" env-required:"true"` //env-required - обязательное поле
	Listen  struct {
		Type   string `yaml:"type" env-default:"port"` //env-default - значение по умолчанию
		BindIP string `yaml:"bind_ip" env-default:"127.0.0.1"`
		Port   string `yaml:"port" env-default:"8080"`
	} `yaml:"listening"`

	MongoDB struct {
		Host       string `json:"host"`
		Port       string `json:"port"`
		Database   string `json:"database"`
		AuthDB     string `json:"authDB"`
		Username   string `json:"username"`
		Password   string `json:"password"`
		Collection string `json:"collection"`
	} `json:"mongodb"`

	Postgres struct {
		Host     string `json:"host"`
		Port     string `json:"port"`
		Database string `json:"database"`
		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"postgres"`
}

var instance *Config
var once sync.Once

func GetConfig() *Config {
	once.Do(func() { // once.Do означает что этот код выполнится только один раз. при повторном вызове фуункции код выполняться не будет, сразу вызовется инстансе
		logger := logging.GetLogger()
		logger.Info("read application configuration")
		instance = &Config{}
		if err := cleanenv.ReadConfig("60_Rest_API/rest-api-tutorial/config.yml", instance); err != nil {
			help, _ := cleanenv.GetDescription(instance, nil)
			logger.Info(help)
			logger.Fatal(err)
		}
	})
	return instance
}
