package postgresql

import (
	"context"
	"fmt"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/Golang_lesson/60_Rest_API/rest-api-tutorial/internal/config"
	"gitlab.com/Golang_lesson/60_Rest_API/rest-api-tutorial/pkg/utils"
	"log"
	"time"
)

type Client interface {
	Exec(ctx context.Context, sql string, arguments ...interface{}) (pgconn.CommandTag, error)
	Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error)
	QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row
	Begin(ctx context.Context) (pgx.Tx, error)
}

func NewClient(ctx context.Context, maxAttempts int, sc config.Config) (pool *pgxpool.Pool, err error) {

	//dsn := fmt.Sprint("postgresql://postgres:postgres@localhost:5432/postgres")
	dsn := fmt.Sprintf("postgresql://%s:%s@%s:%s/%s", sc.Postgres.Username, sc.Postgres.Password, sc.Postgres.Host, sc.Postgres.Port, sc.Postgres.Database)

	err = repeatable.DoWithTries(func() error {
		ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
		defer cancel()

		pool, err = pgxpool.Connect(ctx, dsn)
		if err != nil {
			return err
		}

		return nil
	}, maxAttempts, 5*time.Second)

	if err != nil {
		log.Fatal("error do with tries postgresql")
	}

	return pool, nil
}
