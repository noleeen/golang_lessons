package coincap

import "fmt"

type AssetsResponse struct {
	Asset     Asset `json:"data"`
	Timestamp int64 `json:"timestamp"`
}

type AssetsResponses struct {
	Assets    []Asset `json:"data"`
	Timestamp int64   `json:"timestamp"`
}

type Asset struct {
	ID           string `json:"id"`
	Rank         string `json:"rank"`
	Symbol       string `json:"symbol"`
	Name         string `json:"name"`
	Supply       string `json:"supply"`
	MaxSupply    string `json:"maxSupply"`
	MarketCapUSD string `json:"marketCapUsd"`
	VolumeUSD24h string `json:"volumeUsd24Hr"`
	PriceUSD     string `json:"priceUSD"`
}

func (a Asset) Info() string {
	return fmt.Sprintf("[ID] %s | [RANC] %s | [SYMBOL] %s | [NAME] %s | [PRICE] %s",
		a.ID, a.Rank, a.Symbol, a.Name, a.PriceUSD)
}
