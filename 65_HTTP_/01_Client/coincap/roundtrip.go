package coincap

import (
	"fmt"
	"io"
	"net/http"
	"time"
)

//логирование запросов(выводится инфо перед каждым запросом) с помощью поля Transport (структуры Clien) в которое мы добавили RoundTripper
type loggingRoundTripper struct {
	logger io.Writer
	next   http.RoundTripper
}

func (l loggingRoundTripper) RoundTrip(r *http.Request) (*http.Response, error) {
	fmt.Fprintf(l.logger, "[%s] %s %s\n", time.Now().Format(time.ANSIC), r.Method, r.URL)
	return l.next.RoundTrip(r)
}
