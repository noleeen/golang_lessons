package main

import (
	"fmt"
	"gitlab.com/Golang_lesson/65_HTTP_/01_Client/coincap"
	"log"
	"time"
)

func main() {
	coincapClient, err := coincap.NewClient(10 * time.Second)
	if err != nil {
		log.Fatal(err)
	}
	assets, err := coincapClient.GetAssets("https://api.coincap.io/v2/assets")
	if err != nil {
		log.Fatal()
	}
	for _, v := range assets {
		fmt.Println(v.Info())
	}

}
