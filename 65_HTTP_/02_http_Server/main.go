package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
)

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name,omitempty"`
}

var users = []User{{1, "Vasya"}, {2, "Ivan"}}

func main() {
	http.HandleFunc("/", loggerMiddleware(handlerUsers))
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}

func loggerMiddleware(next http.HandlerFunc) http.HandlerFunc { // создаём функцию-обработчик, которая возвращает http.HandlerFunc
	// чтоб обернуть параметр в ф-ии HandlerFunc (т. е. сначала будет запускаться эта функция и выдавать строку с логом,
	//а потом уже будет запускаться handlerUsers)
	return func(w http.ResponseWriter, r *http.Request) {
		log.Printf("[%s] %s\n", r.Method, r.URL)
		next(w, r)
	}
}

func handlerUsers(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet: // http.MethodGet - это константа равная "GET"
		getUsers(w, r)
	case http.MethodPost:
		addUser(w, r)
	default:
		w.WriteHeader(http.StatusNotImplemented) // записываем в статус-код ответа 501 - ошибка сервера ("эта страница ещё не реализована")
	}
}

func getUsers(w http.ResponseWriter, r *http.Request) {
	resp, err := json.Marshal(users)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Write(resp)
}

func addUser(w http.ResponseWriter, r *http.Request) {
	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var user User

	if err := json.Unmarshal(reqBody, &user); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	users = append(users, user)
}
