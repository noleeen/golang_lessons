package main

import "fmt"

//1. Дано натуральное число n
//. Выведите все числа от 1 до n

func recursion(n int) {
	if n == 0 {
		return
	}

	recursion(n - 1)
	fmt.Println(n)
}

//func main() {
//	recursion(5)
//}
