package main

import "fmt"

//2. Даны два целых числа A и В (каждое в отдельной строке).
//Выведите все числа от A до B включительно, в порядке возрастания, если A < B, или в порядке убывания в противном случае.

func recursion2(a, b int) {
	if a <= b {
		recUP(a, b)
	} else {
		recDOWN(a, b)
	}

}

func recUP(a, b int) {
	if b < a {
		return
	}

	recUP(a, b-1)
	fmt.Println(b)
}

func recDOWN(a, b int) {
	if a < b {
		return
	}
	fmt.Println(a)
	recDOWN(a-1, b)

}

func main() {
	recursion2(5, 1)
}
