package main

import "testing"

func TestMult(t *testing.T) {
	t.Run("simple", func(t *testing.T) {
		var x, y, res = 2, 5, 10
		realRes := Multiply(x, y)
		if realRes != res {
			t.Errorf("realRes %d != res %d", realRes, res)
		}
	})
	t.Run("negative", func(t *testing.T) {
		var x, y, res = 2, -5, -10
		realRes := Multiply(x, y)
		if realRes != res {
			t.Errorf("realRes %d != res %d", realRes, res)
		}
		t.Run("negativeHard", func(t *testing.T) {
			var x, y, res = 222, -55, -12210
			realRes := Multiply(x, y)
			if realRes != res {
				t.Errorf("realRes %d != res %d", realRes, res)
			}
		})

	})
}
