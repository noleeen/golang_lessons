package main

import (
	"fmt"
	"reflect"
	"strings"
)

type Foo struct {
	A int `tag1:"First tag" tag2:"Second tag"`
	B string
}

func main() {
	sl := []int{1, 2, 3, 4}
	greeting := "hello"
	greetingPtr := &greeting
	f := Foo{10, "Salutations"}
	fp := &f

	fmt.Println(reflect.TypeOf(sl))
	fmt.Println(reflect.TypeOf(greeting))
	fmt.Println(reflect.TypeOf(greetingPtr))
	fmt.Println(reflect.TypeOf(f))
	fmt.Println(reflect.TypeOf(fp))

	slType := reflect.TypeOf(sl)
	gType := reflect.TypeOf(greeting)
	gPtrType := reflect.TypeOf(greetingPtr)
	fType := reflect.TypeOf(f)
	fpType := reflect.TypeOf(fp)

	examiner(slType, 0)
	examiner(gType, 0)
	examiner(gPtrType, 0)
	examiner(fType, 0)
	examiner(fpType, 0)

}

func examiner(t reflect.Type, depth int) {
	fmt.Println(strings.Repeat("\t", depth), "Type is", t.Name(), "and kind is", t.Kind())
	switch t.Kind() {
	case reflect.Array, reflect.Chan, reflect.Map, reflect.Slice, reflect.Ptr:
		fmt.Println(strings.Repeat("\t", depth+1), "Contained type: ")
		examiner(t.Elem(), depth+1)
	case reflect.Struct:
		for i := 0; i < t.NumField(); i++ {
			f := t.Field(i) // поле структуры
			fmt.Println(strings.Repeat("\t", depth+1), "Failed", i+1, "name is", f.Name, "type is", f.Type.Name(), "and kind is", f.Type.Kind())
			if f.Tag != "" {
				fmt.Println(strings.Repeat("\t", depth+2), "Tag is", f.Tag)
				fmt.Println(strings.Repeat("\t", depth+2), "tag1 is", f.Tag.Get("tag1"), "tag2 is", f.Tag.Get("tag2"))
			}
		}
	}
}
