package main

import (
	"fmt"
	"reflect"
)

func main() {
	//type MyInt int
	//
	//var x MyInt = 12
	//v := reflect.ValueOf(x)
	//fmt.Println("type:", v.Type().Name())                   // MyInt.
	//fmt.Println(v.Kind())                                   // MyInt.
	//fmt.Println(reflect.TypeOf(x))                          // MyInt.
	//fmt.Println("kind is uint8: ", v.Kind() == reflect.Int) // true.

	// обходим структуру
	//type Assets struct {
	//	A int `json:"doo"`
	//	B string
	//}
	//b := make(map[string]int)
	//b["h"] = 34
	//b["hv"] = 3
	//var x Assets
	//fmt.Println(x)
	//v := reflect.ValueOf(&x)
	//t := reflect.TypeOf(x)
	//vmap := reflect.TypeOf(b)
	//tmap := reflect.ValueOf(&b)
	//fmt.Println(x, v)
	//v.Elem().Field(0).SetInt(99)
	//v.Elem().Field(1_logUserActivity).SetString("ere")
	//fmt.Println(v)
	//fmt.Println("количество полей структуры: ", v.NumField())
	//fmt.Println("имя поля структуры: ", v.Field(0).Kind())
	//fmt.Println("тип поля структуры: ", v.Field(1_logUserActivity).Kind())
	//
	//fmt.Println("имя поля структуры: ", t.Field(0).Name)
	//fmt.Println("тип поля структуры: ", t.Field(0).Type)
	//fmt.Println("тип поля структуры: ", t.Field(0).Type.Name())
	//fmt.Println("тег поля структуры: ", t.Field(0).Tag)
	//fmt.Println("тег поля структуры: ", t.Field(0).Tag.Get("json"))
	//
	//fmt.Println("количество полей структуры: ", vmap.Elem())
	//fmt.Println("количество полей структуры: ", tmap.Elem())
	//-------------------------------------------------------------------------------------------------------
	var x float64 = 3.4
	fmt.Println("value:", reflect.ValueOf(x).String())
	fmt.Println("value:", reflect.ValueOf(x))
}
