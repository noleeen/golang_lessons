package main

import (
	"errors"
	"fmt"
	"strings"
)

func capitalize(name string) (string, error) {
	if name == "" {
		return "", errors.New("no name provided")
	}
	return strings.ToTitle(name), nil
}

func main() {
	name, err := capitalize("summy")
	if err != nil {
		fmt.Println("asdf", err)
		return
	}
	fmt.Println("Capitalazed name:", name)
}
