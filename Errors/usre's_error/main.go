package main

import (
	"errors"
	"fmt"
	"os"
)

type MyError struct {
	StatusCode int
	Err        error
}

func (m *MyError) Error() string { return fmt.Sprintf("status %d: err %v", m.StatusCode, m.Err) }

func doRequest() error {
	return &MyError{StatusCode: 503, Err: errors.New("error's text")}
}

func main() {
	err := doRequest()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
