package main

import (
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", Handler)

	log.Println("Start HTTP server on port 8081")
	log.Fatal(http.ListenAndServe(":8081", nil))
}

func Handler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("good luck, Alesya, Aryna and Sasha!\n"))
}
